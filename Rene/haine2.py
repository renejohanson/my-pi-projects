import sys
import RPi.GPIO as GPIO
import MySQLdb as mysql
import datetime, time, sched

GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT) #led roheline
GPIO.setup(22, GPIO.OUT) #led sinine
GPIO.setup(27, GPIO.OUT) #led punane
GPIO.setup(10, GPIO.OUT) #relay
GPIO.setup(2, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac1 in
GPIO.setup(3, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac2 in
GPIO.setup(4, GPIO.IN, pull_up_down = GPIO.PUD_DOWN) #rotation sensor in
GPIO.setup(18, GPIO.IN, pull_up_down = GPIO.PUD_UP)

roundcount = 0
roundcount_total = 0
serial_number = "0"
ac1_status = not GPIO.input(2)
ac2_status = not GPIO.input(3)
next_call = time.time()
next_interval = 3
s = sched.scheduler(time.time, time.sleep)

def blink_red(): #done
    try:
        GPIO.output(27, True)
        time.sleep(0.05)
        GPIO.output(27, False)
        time.sleep(1)
	
    except KeyboardInterrupt:
        print "\n Klaveri katkestus"
        sys.exit(0)

def remote_server_is_reachable():
    try:
        con = mysql.connect('192.168.1.19', 'root', 'Hullr007', 'haine');    
        cur = con.cursor()
        cur.execute("SELECT VERSION()")
        result = cur.fetchone()
        if (result[0] != ""):
            #print "remote server is reachable"
            return 1
    
    except mysql.Error as err:
        print("ERROR: {}".format(err))

def roundcount_total_value_read(): #done
    try:
        con = mysql.connect('localhost', 'root', 'Hullr007', 'haine');    
        cur = con.cursor()
        cur.execute("SELECT COUNT(1) FROM temp_config WHERE variable = 'roundcount_total'")
        result = cur.fetchone()
        global roundcount_total
        if (result[0] == 0):
            roundcount_total = 0
        else:
            cur.execute("SELECT value FROM temp_config WHERE variable = 'roundcount_total'")
            result = cur.fetchone()
            roundcount_total = result[0]
        
    except mysql.Error as err:
        print("ERROR: {}".format(err))
    
def roundcount_total_value_save(): #done
    try:
        con = mysql.connect('localhost', 'root', 'Hullr007', 'haine');    
        cur = con.cursor()
        cur.execute("SELECT COUNT(1) FROM temp_config WHERE variable = 'roundcount_total'")
        result = cur.fetchone()
        if (result[0] == 0):
            req = "INSERT INTO temp_config (variable, value) VALUES ('roundcount_total', %s)"
            cur.execute(req, (roundcount_total))
            con.commit()
            print "INSERTED roundcount total value into database!"
        else:
            req = "UPDATE temp_config SET value=%s WHERE variable='roundcount_total'"
            cur.execute(req, (roundcount_total))
            con.commit()
            print "roundcount_total = ", roundcount_total
            print "ALTERED roundcount total value in database!"
        
    except mysql.Error as err:
        print("ERROR: {}".format(err))
        
def local_insert():
    try:
        global roundcount
        aeg = (time.strftime("%H:%M:%S"))
        kuupaev = (time.strftime("%d.%m.%Y"))
        con = mysql.connect('localhost', 'root', 'Hullr007', 'haine');	
        cur = con.cursor()
        req = "INSERT INTO local_data (tulem, tulem_kokku, ac1, ac2, serial, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        cur.execute(req, (roundcount, roundcount_total, ac1_status, ac2_status, serial_number, aeg, kuupaev))
        con.commit()
        print "Andmed sisestatud LOKAALSESSE ANDMEBAASI!"
        roundcount_total_value_save()
        roundcount = 0
        
    except mysql.Error as err:
        print("ERROR: {}".format(err))
        
def remote_insert():
    try:
        global roundcount 
        aeg = (time.strftime("%H:%M:%S"))
        kuupaev = (time.strftime("%d.%m.%Y"))
        con = mysql.connect('192.168.1.19', 'root', 'Hullr007', 'haine');    
        cur = con.cursor()
        req = "INSERT INTO local_data (tulem, tulem_kokku, ac1, ac2, serial, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        cur.execute(req, (roundcount, roundcount_total, ac1_status, ac2_status, serial_number, aeg, kuupaev))
        con.commit()
        print "Andmed sisestatud GLOBAALSESSE ANDMEBAASI!"
        roundcount_total_value_save()
        roundcount = 0
        
    except mysql.Error as err:
        print("ERROR: {}".format(err))
        

def check_for_roundcount_total_reset(): #done
#kontrollime kas mysql baasis tabelis temp_config on muutuja roundcount_total_reset vaartus 1, kui on 1, tagastame t6ese vaartuse ja nullime muutuja roundcount_total
    try:
        global roundcount_total
        con = mysql.connect('localhost', 'root', 'Hullr007', 'haine');
        cur = con.cursor()
        cur.execute("SELECT value FROM temp_config WHERE variable = 'roundcount_total_reset'")
        result = cur.fetchone()
        res = int(result[0])
        if (res == 1):
            cur.execute("UPDATE temp_config SET value = 0 WHERE variable = 'roundcount_total_reset' LIMIT 1")
            con.commit()
            roundcount_total = 0
            print "Roundcount total on nullitud!"
    
    except mysql.Error as err:
        print("ERROR: {}".format(err))
	
def check_for_stop_count(): #done
#kontrollime kas mysql baasis tabelis temp_config on muutuja stop_count vaartus suurem kui 0 kui on suurem kui 0 seame muutuja stop_count vordseks selle vaartusega ning vordleme roundcount_total vaartust stop_count vaartusega, kui roundcount_total on vordne voi suurem kui stop_count siis paneme masina seisma
    try:
        global roundcount_total
        con = mysql.connect('localhost', 'root', 'Hullr007', 'haine');	
        cur = con.cursor()
        cur.execute("SELECT value FROM temp_config WHERE variable = 'stop_count'")
        result = cur.fetchone()
        res = int(result[0])
        if (res != 0) and (res <= roundcount_total) :
            GPIO.output(10, True)
            time.sleep(1.5)
            GPIO.output(10, False)
            cur.execute("UPDATE temp_config SET value = 0 WHERE variable = 'stop_count' LIMIT 1")
            con.commit()
            print "Roundcount to stop reached!"
            
    except mysql.Error as err:
        print("ERROR: {}".format(err))
	
def check_for_remote_machine_stop():#done
    #kontrollime kas mysql baasis tabelis temp_config on muutuja remote_machine_stop vaartus suurem kui 0, kui on siis seiskame masina koheselt (lylitame relee sekundiks sisse).
    try:
        con = mysql.connect('localhost', 'root', 'Hullr007', 'haine');	
        cur = con.cursor()
        cur.execute("SELECT value FROM temp_config WHERE variable = 'remote_machine_stop'")
        result = cur.fetchone()
        res = int(result[0])
        if (res	== 1):
            GPIO.output(10, True)
            time.sleep(1.5)
            GPIO.output(10, False)
            cur.execute("UPDATE temp_config SET value = 0 WHERE variable = 'remote_machine_stop' LIMIT 1")
            con.commit()
            print "Machine remotely stopped!"

    except mysql.Error as err:
        print("ERROR: {}".format(err))

def gpio_4(channel):
	global roundcount
	global roundcount_total
	roundcount += 1
	roundcount_total += 1
	
GPIO.add_event_detect(4, GPIO.FALLING, gpio_4, bouncetime=10) #bouncetime=50

try:
    roundcount_total_value_read()
    
    while True:
        try:
            s.enter(3, 1, check_for_roundcount_total_reset, ())
            s.enter(3, 1, check_for_remote_machine_stop, ())
            s.enter(3, 1, check_for_stop_count, ())
            #if kauge server on k2ttesaadav k2ivitame funktsiooni remote_insert() kui kauge server pole k2ttesaadav k2ivitame funktsiooni local_insert() 
            
            if (remote_server_is_reachable() == 1):
                s.enter(3, 1, remote_insert, ())
            else:
                s.enter(3, 1, local_insert, ())
            s.run()
            
            print "----------------------------------------------------------------------- \n"
            
        except KeyboardInterrupt:
            print "\n Klaveri katkestus"
            sys.exit(0)
            
        GPIO.output(17, False)
        GPIO.output(22, False)
        GPIO.output(27, False)
        GPIO.output(10, False)
        
        if (GPIO.input(2) == False) and (GPIO.input(3) == False):
			blink_red() #Katkestus, punane vilkuma
        if (GPIO.input(2) == False) and (GPIO.input(3) == True):
            GPIO.output(17, True) #k6ik on OK, t88 k2ib, roheline p6lema
            time.sleep(0.1)
        if (GPIO.input(2) == True) and (GPIO.input(3) == True):
            GPIO.output(22, True) #masin seisab, kollane p6lema
            time.sleep(0.1)
        if (GPIO.input(2) == True):
            GPIO.output(22, True) #masin seisab, kollane p6lema
            time.sleep(0.1)

except KeyboardInterrupt:
    print "\n Klaveri katkestus"
    sys.exit(0)
    
    
#except:  
    # this catches ALL other exceptions including errors.  
    # You won't get any error messages for debugging  
    # so only use it once your code is working  
 #   print "Mingi muu jant!"  
  
finally:  
    GPIO.cleanup() # this ensures a clean exit  
