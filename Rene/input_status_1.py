import sys
import RPi.GPIO as GPIO
import datetime, time, sched

GPIO.setmode(GPIO.BCM)
#input pin variables
input_220v_1 = 4
input_motor_1 = 12
input_stop_1 = 7
input_rotation_1 = 8
"""output_led_green_1 = 111
output_led_yellow_1 = 112
output_led_red_1 = 113
output_relay_1 = 114"""

input_220v_2 = 14
input_motor_2 = 25
input_stop_2 = 24
input_rotation_2 = 23
"""output_led_green_2 = 115
output_led_yellow_2 = 116
output_led_red_2 = 117
output_relay_2 = 118"""

input_220v_3 = 18
input_motor_3 = 15
input_stop_3 = 14
input_rotation_3 = 16
"""output_led_green_3 = 119
output_led_yellow_3 = 120
output_led_red_3 = 121
output_relay_3 = 122"""

#device 1 GPIO setup
"""GPIO.setup(output_led_green_1, GPIO.OUT)
GPIO.setup(output_led_yellow_1, GPIO.OUT)
GPIO.setup(output_led_red_1, GPIO.OUT)
GPIO.setup(output_relay_1, GPIO.OUT)"""
GPIO.setup(input_220v_1, GPIO.IN)
GPIO.setup(input_motor_1, GPIO.IN)
GPIO.setup(input_stop_1, GPIO.IN)
GPIO.setup(input_rotation_1, GPIO.IN)
i_220v_1 = GPIO.input(input_220v_1)
i_motor_1 = GPIO.input(input_motor_1)
i_stop_1 = GPIO.input(input_stop_1)
i_rotation_1 = GPIO.input(input_rotation_1)

#device 2 GPIO setup
"""GPIO.setup(output_led_green_2, GPIO.OUT)
GPIO.setup(output_led_yellow_2, GPIO.OUT)
GPIO.setup(output_led_red_2, GPIO.OUT)
GPIO.setup(output_relay_2, GPIO.OUT)"""
GPIO.setup(input_220v_2, GPIO.IN)
GPIO.setup(input_motor_2, GPIO.IN)
GPIO.setup(input_stop_2, GPIO.IN)
GPIO.setup(input_rotation_2, GPIO.IN)
i_220v_2 = GPIO.input(input_220v_2)
i_motor_2 = GPIO.input(input_motor_2)
i_stop_2 = GPIO.input(input_stop_2)
i_rotation_2 = GPIO.input(input_rotation_2)

#device 3 GPIO setup
"""GPIO.setup(output_led_green_3, GPIO.OUT)
GPIO.setup(output_led_yellow_3, GPIO.OUT)
GPIO.setup(output_led_red_3, GPIO.OUT)
GPIO.setup(output_relay_3, GPIO.OUT)"""
GPIO.setup(input_220v_3, GPIO.IN)
GPIO.setup(input_motor_3, GPIO.IN)
GPIO.setup(input_stop_3, GPIO.IN)
GPIO.setup(input_rotation_3, GPIO.IN)
i_220v_3 = GPIO.input(input_220v_3)
i_motor_3 = GPIO.input(input_motor_3)
i_stop_3 = GPIO.input(input_stop_3)
i_rotation_3 = GPIO.input(input_rotation_3)

s = sched.scheduler(time.time, time.sleep)

try:
    print i_220v_1
    print i_motor_1
    print i_stop_1
    print i_rotation_1

    print i_220v_2
    print i_motor_2
    print i_stop_2
    print i_rotation_2

    print i_220v_3
    print i_motor_3
    print i_stop_3
    print i_rotation_3

except KeyboardInterrupt:
    print "\n Klaveri katkestus"
    GPIO.cleanup() # this ensures a clean exit
    sys.exit(0)