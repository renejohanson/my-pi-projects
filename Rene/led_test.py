import sys
import RPi.GPIO as GPIO
import MySQLdb as mysql
import datetime, time, sched

GPIO.setmode(GPIO.BCM)
#seade 1
GPIO.setup(27, GPIO.OUT) #led punane!
GPIO.setup(10, GPIO.OUT) #led roheline!
GPIO.setup(9, GPIO.OUT) #led kollane!
GPIO.setup(11, GPIO.OUT) #relay
GPIO.setup(4, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac1 in!
GPIO.setup(17, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac2 in!
GPIO.setup(22, GPIO.IN, pull_up_down = GPIO.PUD_UP) #rotation sensor in

#seade 2
GPIO.setup(25, GPIO.OUT) #led roheline
GPIO.setup(8, GPIO.OUT) #led kollane
GPIO.setup(7, GPIO.OUT) #led punane
GPIO.setup(18, GPIO.OUT) #relay
GPIO.setup(14, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac1 in
GPIO.setup(15, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac2 in
GPIO.setup(23, GPIO.IN, pull_up_down = GPIO.PUD_UP) #rotation sensor in

#seade 3
GPIO.setup(16, GPIO.OUT) #led roheline
GPIO.setup(20, GPIO.OUT) #led kollane
GPIO.setup(21, GPIO.OUT) #led punane
GPIO.setup(6, GPIO.OUT) #relay
GPIO.setup(19, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac1 in
GPIO.setup(26, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac2 in
GPIO.setup(13, GPIO.IN, pull_up_down = GPIO.PUD_UP) #rotation sensor in

while True:
        try:
           GPIO.output(13, True)
           time.sleep(0.05)
           GPIO.output(13, False)
           time.sleep(1) 
            
        except KeyboardInterrupt:
            print "\n Klaveri katkestus"
            sys.exit(0)
            GPIO.cleanup() # this ensures a clean exit