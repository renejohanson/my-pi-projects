import sys
import RPi.GPIO as GPIO
import time, sched

GPIO.setmode(GPIO.BCM)

GPIO.setup(2, GPIO.IN)
GPIO.setup(3, GPIO.IN)
GPIO.setup(4, GPIO.IN)
GPIO.setup(5, GPIO.IN)
GPIO.setup(6, GPIO.IN)
GPIO.setup(7, GPIO.IN)
GPIO.setup(8, GPIO.IN)
GPIO.setup(9, GPIO.IN)
GPIO.setup(10, GPIO.IN)
GPIO.setup(11, GPIO.IN)
GPIO.setup(12, GPIO.IN)
GPIO.setup(13, GPIO.IN)
GPIO.setup(14, GPIO.IN)
GPIO.setup(15, GPIO.IN)
GPIO.setup(16, GPIO.IN)
GPIO.setup(17, GPIO.IN)
GPIO.setup(18, GPIO.IN)
GPIO.setup(19, GPIO.IN)
GPIO.setup(20, GPIO.IN)
GPIO.setup(21, GPIO.IN)
GPIO.setup(22, GPIO.IN)
GPIO.setup(23, GPIO.IN)
GPIO.setup(24, GPIO.IN)
GPIO.setup(25, GPIO.IN)
GPIO.setup(26, GPIO.IN)
GPIO.setup(27, GPIO.IN)

roundcount_2 = 0
roundcount_3 = 0
roundcount_4 = 0
roundcount_5 = 0
roundcount_6 = 0
roundcount_7 = 0
roundcount_8 = 0
roundcount_9 = 0
roundcount_10 = 0
roundcount_11 = 0
roundcount_12 = 0
roundcount_13 = 0
roundcount_14 = 0
roundcount_15 = 0
roundcount_16 = 0
roundcount_17 = 0
roundcount_18 = 0
roundcount_19 = 0
roundcount_20 = 0
roundcount_21 = 0
roundcount_22 = 0
roundcount_23 = 0
roundcount_24 = 0
roundcount_25 = 0
roundcount_26 = 0
roundcount_27 = 0

s = sched.scheduler(time.time, time.sleep)

def gpio_2(channel):
    global roundcount_2
    roundcount_2 += 1

def gpio_3(channel):
    global roundcount_3
    roundcount_3 += 1
    
def gpio_4(channel):
    global roundcount_4
    roundcount_4 += 1
    
def gpio_5(channel):
    global roundcount_5
    roundcount_5 += 1
    
def gpio_6(channel):
    global roundcount_6
    roundcount_6 += 1
    
def gpio_7(channel):
    global roundcount_7
    roundcount_7 += 1
    
def gpio_8(channel):
    global roundcount_8
    roundcount_8 += 1
    
def gpio_9(channel):
    global roundcount_9
    roundcount_9 += 1
    
def gpio_10(channel):
    global roundcount_10
    roundcount_10 += 1
    
def gpio_11(channel):
    global roundcount_11
    roundcount_11 += 1
    
def gpio_12(channel):
    global roundcount_12
    roundcount_12 += 1
    
def gpio_13(channel):
    global roundcount_13
    roundcount_13 += 1
    
def gpio_13(channel):
    global roundcount_13
    roundcount_13 += 1
    
def gpio_14(channel):
    global roundcount_14
    roundcount_14 += 1
    
def gpio_15(channel):
    global roundcount_15
    roundcount_15 += 1
    
def gpio_15(channel):
    global roundcount_15
    roundcount_15 += 1
    
def gpio_16(channel):
    global roundcount_16
    roundcount_16 += 1
    
def gpio_17(channel):
    global roundcount_17
    roundcount_17 += 1
    
def gpio_18(channel):
    global roundcount_18
    roundcount_18 += 1
    
def gpio_19(channel):
    global roundcount_19
    roundcount_19 += 1
    
def gpio_20(channel):
    global roundcount_20
    roundcount_20 += 1
    
def gpio_21(channel):
    global roundcount_21
    roundcount_21 += 1
    
def gpio_22(channel):
    global roundcount_22
    roundcount_22 += 1
    
def gpio_23(channel):
    global roundcount_23
    roundcount_23 += 1
    
def gpio_24(channel):
    global roundcount_24
    roundcount_24 += 1
    
def gpio_25(channel):
    global roundcount_25
    roundcount_25 += 1
    
def gpio_26(channel):
    global roundcount_26
    roundcount_26 += 1
    
def gpio_27(channel):
    global roundcount_27
    roundcount_27 += 1
    
GPIO.add_event_detect(2, GPIO.FALLING, gpio_2)
GPIO.add_event_detect(3, GPIO.FALLING, gpio_3)
GPIO.add_event_detect(4, GPIO.FALLING, gpio_4)
GPIO.add_event_detect(5, GPIO.FALLING, gpio_5)
GPIO.add_event_detect(6, GPIO.FALLING, gpio_6)
GPIO.add_event_detect(7, GPIO.FALLING, gpio_7)
GPIO.add_event_detect(8, GPIO.FALLING, gpio_8)
GPIO.add_event_detect(9, GPIO.FALLING, gpio_9)
GPIO.add_event_detect(10, GPIO.FALLING, gpio_10)
GPIO.add_event_detect(11, GPIO.FALLING, gpio_11)
GPIO.add_event_detect(12, GPIO.FALLING, gpio_12)
GPIO.add_event_detect(13, GPIO.FALLING, gpio_13)
GPIO.add_event_detect(14, GPIO.FALLING, gpio_14)
GPIO.add_event_detect(15, GPIO.FALLING, gpio_15)
GPIO.add_event_detect(16, GPIO.FALLING, gpio_16)
GPIO.add_event_detect(17, GPIO.FALLING, gpio_17)
GPIO.add_event_detect(18, GPIO.FALLING, gpio_18)
GPIO.add_event_detect(19, GPIO.FALLING, gpio_19)
GPIO.add_event_detect(20, GPIO.FALLING, gpio_20)
GPIO.add_event_detect(21, GPIO.FALLING, gpio_21)
GPIO.add_event_detect(22, GPIO.FALLING, gpio_22)
GPIO.add_event_detect(23, GPIO.FALLING, gpio_23)
GPIO.add_event_detect(24, GPIO.FALLING, gpio_24)
GPIO.add_event_detect(25, GPIO.FALLING, gpio_25)
GPIO.add_event_detect(26, GPIO.FALLING, gpio_26)
GPIO.add_event_detect(27, GPIO.FALLING, gpio_27)

def print_input_status():
    global roundcount_2
    global roundcount_3
    global roundcount_4
    global roundcount_5
    global roundcount_6
    global roundcount_7
    global roundcount_8
    global roundcount_9
    global roundcount_10
    global roundcount_11
    global roundcount_12
    global roundcount_13
    global roundcount_14
    global roundcount_15
    global roundcount_16
    global roundcount_17
    global roundcount_18
    global roundcount_19
    global roundcount_20
    global roundcount_21
    global roundcount_22
    global roundcount_23
    global roundcount_24
    global roundcount_25
    global roundcount_26
    global roundcount_27
    
    print "ROUNDCOUNT_2:   | ", roundcount_2, " | ROUNDCOUNT_3:   | ", roundcount_3, " |"
    print "ROUNDCOUNT_4:   | ", roundcount_4, " | ROUNDCOUNT_5:   | ", roundcount_5, " |"
    print "ROUNDCOUNT_6:   | ", roundcount_6, " | ROUNDCOUNT_7:   | ", roundcount_7, " |"
    print "ROUNDCOUNT_8:   | ", roundcount_8, " | ROUNDCOUNT_9:   | ", roundcount_9, " |"
    print "ROUNDCOUNT_10:   | ", roundcount_10, " | ROUNDCOUNT_11:   | ", roundcount_11, " |"
    print "ROUNDCOUNT_12:   | ", roundcount_12, " | ROUNDCOUNT_13:   | ", roundcount_13, " |"
    print "ROUNDCOUNT_14:   | ", roundcount_14, " | ROUNDCOUNT_15:   | ", roundcount_15, " |"
    print "ROUNDCOUNT_16:   | ", roundcount_16, " | ROUNDCOUNT_17:   | ", roundcount_17, " |"
    print "ROUNDCOUNT_18:   | ", roundcount_18, " | ROUNDCOUNT_19:   | ", roundcount_19, " |"
    print "ROUNDCOUNT_20:   | ", roundcount_20, " | ROUNDCOUNT_21:   | ", roundcount_21, " |"
    print "ROUNDCOUNT_22:   | ", roundcount_22, " | ROUNDCOUNT_23:   | ", roundcount_23, " |"
    print "ROUNDCOUNT_24:   | ", roundcount_24, " | ROUNDCOUNT_25:   | ", roundcount_25, " |"
    print "ROUNDCOUNT_26:   | ", roundcount_26, " | ROUNDCOUNT_27:   | ", roundcount_27, " |"
    
    roundcount_2 = 0
    roundcount_3 = 0
    roundcount_4 = 0
    roundcount_5 = 0
    roundcount_6 = 0
    roundcount_7 = 0
    roundcount_8 = 0
    roundcount_9 = 0
    roundcount_10 = 0
    roundcount_11 = 0
    roundcount_12 = 0
    roundcount_13 = 0
    roundcount_14 = 0
    roundcount_15 = 0
    roundcount_16 = 0
    roundcount_17 = 0
    roundcount_18 = 0
    roundcount_19 = 0
    roundcount_20 = 0
    roundcount_21 = 0
    roundcount_22 = 0
    roundcount_23 = 0
    roundcount_24 = 0
    roundcount_25 = 0
    roundcount_26 = 0
    roundcount_27 = 0

try:
    while True:
        try:
            #print_input_status()
            s.enter(1, 1, print_input_status, ())
            s.run()
            
        except KeyboardInterrupt:
            print "\n Klaveri katkestus"
            GPIO.cleanup() # this ensures a clean exit
            sys.exit(0)

except KeyboardInterrupt:
            print "\n Klaveri katkestus"
            GPIO.cleanup() # this ensures a clean exit
            sys.exit(0)