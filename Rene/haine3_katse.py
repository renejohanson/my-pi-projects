import sys
import RPi.GPIO as GPIO
import MySQLdb as mysql
import datetime, time, sched

GPIO.setmode(GPIO.BCM)
#seade 1
GPIO.setup(27, GPIO.OUT) #led roheline
GPIO.setup(10, GPIO.OUT) #led kollane
GPIO.setup(9, GPIO.OUT) #led punane
GPIO.setup(11, GPIO.OUT) #relay
GPIO.setup(4, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac1 in
GPIO.setup(17, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac2 in
GPIO.setup(22, GPIO.IN, pull_up_down = GPIO.PUD_UP) #rotation sensor in

#seade 2
GPIO.setup(25, GPIO.OUT) #led roheline
GPIO.setup(8, GPIO.OUT) #led kollane
GPIO.setup(7, GPIO.OUT) #led punane
GPIO.setup(23, GPIO.OUT) #relay
GPIO.setup(14, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac1 in
GPIO.setup(15, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac2 in
GPIO.setup(23, GPIO.IN, pull_up_down = GPIO.PUD_UP) #rotation sensor in

#seade 3
GPIO.setup(16, GPIO.OUT) #led roheline
GPIO.setup(20, GPIO.OUT) #led kollane
GPIO.setup(21, GPIO.OUT) #led punane
GPIO.setup(6, GPIO.OUT) #relay
GPIO.setup(19, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac1 in
GPIO.setup(26, GPIO.IN, pull_up_down = GPIO.PUD_UP) #ac2 in
GPIO.setup(13, GPIO.IN, pull_up_down = GPIO.PUD_UP) #rotation sensor in

#yldised muutujad
serial_number1 = "5"
serial_number2 = "6"
serial_number3 = "7"
remote_server = '192.168.1.19'
database = 'haine'

#seade 1
roundcount_1 = 0
roundcount_total_1 = 0
ac1_status_1 = not GPIO.input(4)
ac2_status_1 = not GPIO.input(17)

#seade 2
roundcount_2 = 0
roundcount_total_2 = 0
ac1_status_2 = not GPIO.input(14)
ac2_status_2 = not GPIO.input(15)

#seade 3
roundcount_3 = 0
roundcount_total_3 = 0
ac1_status_3 = not GPIO.input(19)
ac2_status_3 = not GPIO.input(26)

def gpio_22(channel):
    global roundcount_1
    global roundcount_total_1
    roundcount_1 += 1
    roundcount_total_1 += 1

def gpio_23(channel):
    global roundcount_2
    global roundcount_total_2
    roundcount_2 += 1
    roundcount_total_2 += 1

def gpio_13(channel):
    global roundcount_3
    global roundcount_total_3
    roundcount_3 += 1
    roundcount_total_3 += 1

GPIO.add_event_detect(22, GPIO.FALLING, gpio_22, bouncetime=10)
GPIO.add_event_detect(23, GPIO.FALLING, gpio_23, bouncetime=10)
GPIO.add_event_detect(13, GPIO.FALLING, gpio_13, bouncetime=10) 

def blink_red1(): #done
    try:
        GPIO.output(9, True)
        time.sleep(0.05)
        GPIO.output(9, False)
        time.sleep(1)
    
    except KeyboardInterrupt:
        print "\n Klaveri katkestus"
        sys.exit(0)

def blink_red2(): #done
    try:
        GPIO.output(7, True)
        time.sleep(0.05)
        GPIO.output(7, False)
        time.sleep(1)

    except KeyboardInterrupt:
        print "\n Klaveri katkestus"
        sys.exit(0)

def blink_red3(): #done
    try:
        GPIO.output(21, True)
        time.sleep(0.05)
        GPIO.output(21, False)
        time.sleep(1)

    except KeyboardInterrupt:
        print "\n Klaveri katkestus"
        sys.exit(0)

def leds_1():
    try:
        GPIO.output(27, False)
        GPIO.output(10, False)
        GPIO.output(9, False)
        GPIO.output(11, False)
        if (GPIO.input(4) == False) and (GPIO.input(17) == False):
            blink_red() #katkestus, punane vilkuma
        if (GPIO.input(4) == False) and (GPIO.input(17) == True):
            GPIO.output(27, True) #k6ik on OK, t88 k2ib, roheline p6lema
            time.sleep(0.1)
        if (GPIO.input(4) == True) and (GPIO.input(17) == True):
            GPIO.output(10, True) #masin seisab, kollame p6lema
            time.sleep(0.1)
        if (GPIO.input(4) == True):
            GPIO.output(10, True) #masin seisab, kollane p6lema
            time.sleep(0.1)
    
    except KeyboardInterrupt:
        print "\n Klaveri katkestus"
        sys.exit(0)