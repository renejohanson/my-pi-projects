
#seade 1
roundcount_1 = 0
roundcount_total_1 = 0
ac1_status_1 = not GPIO.input(4)
ac2_status_1 = not GPIO.input(12)

#seade 2
roundcount_2 = 0
roundcount_total_2 = 0
ac1_status_2 = not GPIO.input(14)
ac2_status_2 = not GPIO.input(25)

#seade 3
roundcount_3 = 0
roundcount_total_3 = 0
ac1_status_3 = not GPIO.input(18)
ac2_status_3 = not GPIO.input(15)

def gpio_22(channel):
    global roundcount_1
    global roundcount_total_1
    roundcount_1 += 1
    roundcount_total_1 += 1

def gpio_23(channel):
    global roundcount_2
    global roundcount_total_2
    roundcount_2 += 1
    roundcount_total_2 += 1

def gpio_13(channel):
    global roundcount_3
    global roundcount_total_3
    roundcount_3 += 1
    roundcount_total_3 += 1
    
GPIO.add_event_detect(8, GPIO.FALLING, gpio_22, bouncetime=10)
GPIO.add_event_detect(23, GPIO.FALLING, gpio_23, bouncetime=10)
GPIO.add_event_detect(16, GPIO.FALLING, gpio_13, bouncetime=10) 

def print_input_status():
    global roundcount_1
    global roundcount_2
    global roundcount_3
    
    print "------------------------------------------------------------------------------"
    print "SEADE:        |   1   |   2   |   3   |"
    print "------------------------------------------------------------------------------"
    print "ROUNDCOUNT:   | ", roundcount_1, " | ", roundcount_2, " | ", roundcount_3, " | "
    print "------------------------------------------------------------------------------"

    #print "AC1:          | ", ac1_status_1, " | ", ac1_status_2(), " | ", ac1_status_3, " | "
    #print "------------------------------------------------------------------------------"
    #print "AC2:          | ", ac2_status_1, " | ", ac2_status_2, " | ", ac2_status_3, " | "
    if(not GPIO.input(4) == False):
        print "ac1_status_1 = false"
        print ac1_status_1
    if(not GPIO.input(4) == True):
        print "ac1_status_1 = true"
        print ac1_status_1
        
    if(not GPIO.input(14) == False):
        print "ac1_status_2 = false"
        print ac1_status_2
    if(not GPIO.input(14) == True):
        print "ac1_status_2 = true"
        print ac1_status_2
        
    if(not GPIO.input(19) == False):
        print "ac1_status_3 = false"
        print ac1_status_3
    if(not GPIO.input(19) == True):
        print "ac1_status_3 = true"
        print ac1_status_3
        
    if(not GPIO.input(17) == False):
        print "ac2_status_1 = false"
        print ac2_status_1
    if(not GPIO.input(17) == True):
        print "ac2_status_1 = true"
        print ac2_status_1
        
    if(not GPIO.input(15) == False):
        print "ac2_status_2 = false"
        print ac2_status_2
    if(not GPIO.input(15) == True):
        print "ac2_status_2 = true"
        print ac2_status_1
        
    if(not GPIO.input(26) == False):
        print "ac2_status_3 = false"
        print ac2_status_3
    if(not GPIO.input(26) == True):
        print "ac2_status_3 = true"
        print ac2_status_3

    #print "AC1:          | ", ac1_status_1, " | ", ac1_status_2, " | ", ac1_status_3, " | "
    #print "------------------------------------------------------------------------------"
    #print "AC2:          | ", ac2_status_1, " | ", ac2_status_2, " | ", ac2_status_3, " | "
    
    roundcount_1 = 0
    roundcount_2 = 0
    roundcount_3 = 0

try:
    while True:
        try:
            #print_input_status()
            s.enter(1, 1, print_input_status, ())
            s.run()
            
        except KeyboardInterrupt:
            print "\n Klaveri katkestus"
            GPIO.cleanup() # this ensures a clean exit
            sys.exit(0)