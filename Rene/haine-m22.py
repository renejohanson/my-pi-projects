import os
import sys
import RPi.GPIO as GPIO
import MySQLdb as mysql
import datetime, time, sched

GPIO.setmode(GPIO.BCM)
#seade 1
GPIO.setup(10, GPIO.OUT) #led roheline
GPIO.setup(9, GPIO.OUT) #led kollane
GPIO.setup(27, GPIO.OUT) #led punane
GPIO.setup(11, GPIO.OUT) #relay
GPIO.setup(4, GPIO.IN) #ac1 in
GPIO.setup(17, GPIO.IN) #ac2 in
GPIO.setup(22, GPIO.IN) #rotation sensor in

#seade 2
GPIO.setup(25, GPIO.OUT) #led roheline
GPIO.setup(8, GPIO.OUT) #led kollane
GPIO.setup(7, GPIO.OUT) #led punane
GPIO.setup(18, GPIO.OUT) #relay
GPIO.setup(14, GPIO.IN) #ac1 in
GPIO.setup(15, GPIO.IN) #ac2 in
GPIO.setup(23, GPIO.IN) #rotation sensor in

#seade 3
GPIO.setup(16, GPIO.OUT) #led roheline
GPIO.setup(20, GPIO.OUT) #led kollane
GPIO.setup(21, GPIO.OUT) #led punane
GPIO.setup(6, GPIO.OUT) #relay
GPIO.setup(19, GPIO.IN) #ac1 in
GPIO.setup(26, GPIO.IN) #ac2 in
GPIO.setup(13, GPIO.IN) #rotation sensor in

#yldised muutujad
serial_number_1 = "4"
serial_number_2 = "6"
serial_number_3 = "5"
remote_server = '192.168.1.19'
remote_server_mysql_user = 'rene'
remote_server_mysql_password = 'Hullr007'
remote_server_database = 'haine'
local_server_mysql_user = 'root'
local_server_mysql_password = 'Hullr007'
local_server_database = 'haine'

s = sched.scheduler(time.time, time.sleep)

#seade 1
roundcount_1 = 0
roundcount_total_1 = 0
ac1_status_1 = not GPIO.input(4)
ac2_status_1 = not GPIO.input(17)

#seade 2
roundcount_2 = 0
roundcount_total_2 = 0
ac1_status_2 = not GPIO.input(14)
ac2_status_2 = not GPIO.input(15)

#seade 3
roundcount_3 = 0
roundcount_total_3 = 0
ac1_status_3 = not GPIO.input(19)
ac2_status_3 = not GPIO.input(26)

def gpio_22(channel):
    global roundcount_1
    global roundcount_total_1
    roundcount_1 += 1
    roundcount_total_1 += 1

def gpio_23(channel):
    global roundcount_2
    global roundcount_total_2
    roundcount_2 += 1
    roundcount_total_2 += 1

def gpio_13(channel):
    global roundcount_3
    global roundcount_total_3
    roundcount_3 += 1
    roundcount_total_3 += 1

GPIO.add_event_detect(22, GPIO.FALLING, gpio_22, bouncetime=10)
GPIO.add_event_detect(23, GPIO.FALLING, gpio_23, bouncetime=10)
GPIO.add_event_detect(13, GPIO.FALLING, gpio_13, bouncetime=10) 

def blink_red_1(): #done
    try:
        GPIO.output(27, True)
        time.sleep(0.05)
        GPIO.output(27, False)
        time.sleep(0.5)
    
    except KeyboardInterrupt:
        print "\n Klaveri katkestus"
        sys.exit(0)

def blink_red_2(): #done
    try:
        GPIO.output(7, True)
        time.sleep(0.05)
        GPIO.output(7, False)
        time.sleep(0.5)

    except KeyboardInterrupt:
        print "\n Klaveri katkestus"
        sys.exit(0)

def blink_red_3(): #done
    try:
        GPIO.output(21, True)
        time.sleep(0.05)
        GPIO.output(21, False)
        time.sleep(0.5)

    except KeyboardInterrupt:
        print "\n Klaveri katkestus"
        sys.exit(0)

def leds_1():
    try:
        GPIO.output(27, False)
        GPIO.output(10, False)
        GPIO.output(9, False)
        GPIO.output(11, False)
        if (GPIO.input(4) == False) and (GPIO.input(17) == False):
            blink_red_1() #Katkestus, punane vilkuma
        if (GPIO.input(4) == False) and (GPIO.input(17) == True):
            GPIO.output(10, True) #k6ik on OK, t88 k2ib, roheline p6lema
            time.sleep(0.1)
        if (GPIO.input(4) == True) and (GPIO.input(17) == True):
            GPIO.output(9, True) #masin seisab, kollane p6lema
            time.sleep(0.1)
        if (GPIO.input(4) == True):
            GPIO.output(9, True) #masin seisab, kollane p6lema
            time.sleep(0.1)
    
    except KeyboardInterrupt:
        print "\n Klaveri katkestus"
        sys.exit(0)

def leds_2():
    try:
        GPIO.output(25, False)
        GPIO.output(8, False)
        GPIO.output(7, False)
        GPIO.output(18, False)
        if (GPIO.input(14) == False) and (GPIO.input(15) == False):
            blink_red_2() #Katkestus, punane vilkuma
        if (GPIO.input(14) == False) and (GPIO.input(15) == True):
            GPIO.output(25, True) #k6ik on OK, t88 k2ib, roheline p6lema
            time.sleep(0.1)
        if (GPIO.input(14) == True) and (GPIO.input(15) == True):
            GPIO.output(8, True) #masin seisab, kollane p6lema
            time.sleep(0.1)
        if (GPIO.input(14) == True):
            GPIO.output(8, True) #masin seisab, kollane p6lema
            time.sleep(0.1)

    except KeyboardInterrupt:
        print "\n Klaveri katkestus"
        sys.exit(0)

def leds_3():
    try:
        GPIO.output(16, False)
        GPIO.output(20, False)
        GPIO.output(21, False)
        GPIO.output(6, False)
        if (GPIO.input(19) == False) and (GPIO.input(26) == False):
            blink_red_3() #Katkestus, punane vilkuma
        if (GPIO.input(19) == False) and (GPIO.input(26) == True):
            GPIO.output(16, True) #k6ik on OK, t88 k2ib, roheline p6lema
            time.sleep(0.1)
        if (GPIO.input(19) == True) and (GPIO.input(26) == True):
            GPIO.output(20, True) #masin seisab, kollane p6lema
            time.sleep(0.1)
        if (GPIO.input(19) == True):
            GPIO.output(20, True) #masin seisab, kollane p6lema
            time.sleep(0.1)
    
    except KeyboardInterrupt:
        print "\n Klaveri katkestus"
        sys.exit(0)

def remote_server_is_reachable(): #done
    try:
        con = mysql.connect(remote_server, remote_server_mysql_user, remote_server_mysql_password, remote_server_database);    
        cur = con.cursor()
        cur.execute("SELECT VERSION()")
        result = cur.fetchone()
        if (result[0] != ""):
            #print "remote server is reachable"
            return 1

    except mysql.Error as err:
        print("ERROR: {}".format(err))

def roundcount_total_value_read(): #done
    try:
        global roundcount_total_1
        global roundcount_total_2
        global roundcount_total_3
        #loome mysql yhenduse
        con = mysql.connect('localhost', local_server_mysql_user, local_server_mysql_password, local_server_database);    
        cur = con.cursor()
            
        #masin 1
        cur.execute("SELECT COUNT(1) FROM temp_config WHERE variable = 'roundcount_total_1'")
        result = cur.fetchone()
        if (result[0] == 0):
                roundcount_total_1 = 0
        else:
            cur.execute("SELECT value FROM temp_config WHERE variable = 'roundcount_total_1'")
            result = cur.fetchone()
            roundcount_total_1 = result[0]
      
        #masin 2
        cur.execute("SELECT COUNT(1) FROM temp_config WHERE variable = 'roundcount_total_2'")
        result = cur.fetchone()
        if (result[0] == 0):
            roundcount_total_2 = 0
        else:
            cur.execute("SELECT value FROM temp_config WHERE variable = 'roundcount_total_2'")
            result = cur.fetchone()
            roundcount_total_2 = result[0]
    
        #masin 3
        cur.execute("SELECT COUNT(1) FROM temp_config WHERE variable = 'roundcount_total_3'")
        result = cur.fetchone()
        if (result[0] == 0):
            roundcount_total_3 = 0
        else:
            cur.execute("SELECT value FROM temp_config WHERE variable = 'roundcount_total_3'")
            result = cur.fetchone()
            roundcount_total_3 = result[0]

        """for x in range(1, 4):
            y = str(x)
            z = 'roundcount_total_' + y
            con = mysql.connect('localhost', local_server_mysql_user, local_server_mysql_password, local_server_database);    
            cur = con.cursor()
            req = "SELECT COUNT(1) FROM temp_config WHERE variable = '' %s"
            cur.execute(req, (z))
            result = cur.fetchone()
            if (result[0] == 0):
                z = 0
            else:
                req = "SELECT value FROM temp_config WHERE variable = '' %s"
                cur.execute(req, (z))
                result = cur.fetchone()
                #exec(z + " = 'asdf'" )
                
                print result[0]
                q = str(result[0])
                w = " = ".join((z, q))
                print w
                #q = str(result[0])
                #exec(z + " = " + q)
                #print roundcount_total_ + y"""
        
    except mysql.Error as err:
        print("ERROR: {}".format(err))
    
def roundcount_total_value_save(): 
    try:
        con = mysql.connect('localhost', local_server_mysql_user, local_server_mysql_password, local_server_database);    
        cur = con.cursor()
        
        #masin 1
        cur.execute("SELECT COUNT(1) FROM temp_config WHERE variable = 'roundcount_total_1'")
        result = cur.fetchone()
        if (result[0] == 0):
            req = "INSERT INTO temp_config (variable, value) VALUES ('roundcount_total_1', %s)"
            cur.execute(req, (roundcount_total_1))
            con.commit()
            #print "INSERTED roundcount total value into database!"
        else:
            req = "UPDATE temp_config SET value=%s WHERE variable='roundcount_total_1'"
            cur.execute(req, (roundcount_total_1))
            con.commit()
            #print "roundcount_total_1 = ", roundcount_total_1
            #print "ALTERED roundcount total value in database!"

        #masin 2
        cur.execute("SELECT COUNT(1) FROM temp_config WHERE variable = 'roundcount_total_2'")
        result = cur.fetchone()
        if (result[0] == 0):
            req = "INSERT INTO temp_config (variable, value) VALUES ('roundcount_total_2', %s)"
            cur.execute(req, (roundcount_total_2))
            con.commit()
            #print "INSERTED roundcount total value into database!"
        else:
            req = "UPDATE temp_config SET value=%s WHERE variable='roundcount_total_2'"
            cur.execute(req, (roundcount_total_2))
            con.commit()
            #print "roundcount_total_2 = ", roundcount_total_2
            #print "ALTERED roundcount total value in database!"

        #masin 2
        cur.execute("SELECT COUNT(1) FROM temp_config WHERE variable = 'roundcount_total_3'")
        result = cur.fetchone()
        if (result[0] == 0):
            req = "INSERT INTO temp_config (variable, value) VALUES ('roundcount_total_3', %s)"
            cur.execute(req, (roundcount_total_3))
            con.commit()
            #print "INSERTED roundcount total value into database!"
        else:
            req = "UPDATE temp_config SET value=%s WHERE variable='roundcount_total_3'"
            cur.execute(req, (roundcount_total_3))
            con.commit()
            #print "roundcount_total_3 = ", roundcount_total_3
            #print "ALTERED roundcount total value in database!"

    except mysql.Error as err:
        print("ERROR: {}".format(err))
        
def local_insert():
    try:
        global roundcount_1
        global roundcount_2
        global roundcount_3
        aeg = (time.strftime("%H:%M:%S"))
        kuupaev = (time.strftime("%d.%m.%Y"))
        con = mysql.connect('localhost', local_server_mysql_user, local_server_mysql_password, local_server_database);    
        cur = con.cursor()
        req1 = "INSERT INTO local_data (tulem, tulem_kokku, ac1, ac2, serial, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        cur.execute(req1, (roundcount_1, roundcount_total_1, ac1_status_1, ac2_status_1, serial_number_1, aeg, kuupaev))
        req2 = "INSERT INTO local_data (tulem, tulem_kokku, ac1, ac2, serial, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        cur.execute(req2, (roundcount_2, roundcount_total_2, ac1_status_2, ac2_status_2, serial_number_2, aeg, kuupaev))
        req3 = "INSERT INTO local_data (tulem, tulem_kokku, ac1, ac2, serial, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        cur.execute(req3, (roundcount_3, roundcount_total_3, ac1_status_3, ac2_status_3, serial_number_3, aeg, kuupaev))
        con.commit()
        #print "Andmed sisestatud LOKAALSESSE ANDMEBAASI!"
        roundcount_total_value_save()
        #siin v6iks olla veel mingi kontroll enne muutuja nullimist, kas info ikka j6udis baasi
        roundcount_1 = 0
        roundcount_2 = 0
        roundcount_3 = 0
        
    except mysql.Error as err:
        print("ERROR: {}".format(err))
        
def remote_insert():
    try:
        global roundcount_1
        global roundcount_2
        global roundcount_3 
        aeg = (time.strftime("%H:%M:%S"))
        kuupaev = (time.strftime("%d.%m.%Y"))
        con = mysql.connect(remote_server, remote_server_mysql_user, remote_server_mysql_password, remote_server_database);    
        cur = con.cursor()
        req = "INSERT INTO local_data (tulem, tulem_kokku, ac1, ac2, serial, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        cur.execute(req, (roundcount_1, roundcount_total_1, ac1_status_1, ac2_status_1, serial_number_1, aeg, kuupaev))
        req = "INSERT INTO local_data (tulem, tulem_kokku, ac1, ac2, serial, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        cur.execute(req, (roundcount_2, roundcount_total_2, ac1_status_2, ac2_status_2, serial_number_2, aeg, kuupaev))
        req = "INSERT INTO local_data (tulem, tulem_kokku, ac1, ac2, serial, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        cur.execute(req, (roundcount_3, roundcount_total_3, ac1_status_3, ac2_status_3, serial_number_3, aeg, kuupaev))
        con.commit()
        #print "Andmed sisestatud GLOBAALSESSE ANDMEBAASI!"
        roundcount_total_value_save()
        #siin v6iks olla veel mingi kontroll enne muutuja nullimist, kas info ikka j6udis baasi
        roundcount_1 = 0
        roundcount_2 = 0
        roundcount_3 = 0
        
    except mysql.Error as err:
        print("ERROR: {}".format(err))
        

def check_for_roundcount_total_reset(): #VAJA TEHA lisada kontroll kas baasis eksisteerivad muutujad roundcount_total_value_reset_1 kuni 3
    #kontrollime kas mysql baasis tabelis temp_config on muutuja roundcount_total_reset vaartus 1, kui on 1, tagastame t6ese vaartuse ja nullime muutuja roundcount_total
    try:
        global roundcount_total_1
        global roundcount_total_2
        global roundcount_total_3
        
        con = mysql.connect('localhost', local_server_mysql_user, local_server_mysql_password, local_server_database);
        cur = con.cursor()
        
        #masin 1
        cur.execute("SELECT value FROM temp_config WHERE variable = 'roundcount_total_reset_1'")
        result = cur.fetchone()
        res = int(result[0])
        if (res == 1):
            cur.execute("UPDATE temp_config SET value = 0 WHERE variable = 'roundcount_total_reset_1' LIMIT 1")
            con.commit()
            roundcount_total_1 = 0
            print "Roundcount total on nullitud!"

        #masin 2
        cur.execute("SELECT value FROM temp_config WHERE variable = 'roundcount_total_reset_2'")
        result = cur.fetchone()
        res = int(result[0])
        if (res == 1):
            cur.execute("UPDATE temp_config SET value = 0 WHERE variable = 'roundcount_total_reset_2' LIMIT 1")
            con.commit()
            roundcount_total_2 = 0
            print "Roundcount total on nullitud!"

        #masin 3
        cur.execute("SELECT value FROM temp_config WHERE variable = 'roundcount_total_reset_3'")
        result = cur.fetchone()
        res = int(result[0])
        if (res == 1):
            cur.execute("UPDATE temp_config SET value = 0 WHERE variable = 'roundcount_total_reset_3' LIMIT 1")
            con.commit()
            roundcount_total_3 = 0
            print "Roundcount total on nullitud!"
    
    except mysql.Error as err:
        print("ERROR: {}".format(err))
	
def check_for_stop_count(): #VAJA TEHA lisada kontroll kas baasis on olemas kontrollitav muutuja
    #kontrollime kas mysql baasis tabelis temp_config on muutuja stop_count vaartus suurem kui 0 kui on suurem kui 0 seame muutuja stop_count vordseks selle vaartusega ning vordleme roundcount_total vaartust stop_count vaartusega, kui roundcount_total on vordne voi suurem kui stop_count siis paneme masina seisma
    try:
        con = mysql.connect('localhost', local_server_mysql_user, local_server_mysql_password, local_server_database);	
        cur = con.cursor()
        
        #masin 1
        cur.execute("SELECT value FROM temp_config WHERE variable = 'stop_count_1'")
        result = cur.fetchone()
        res = int(result[0])
        if (res != 0) and (res <= roundcount_total_1) :
            GPIO.output(10, True)
            time.sleep(1.5)
            GPIO.output(10, False)
            cur.execute("UPDATE temp_config SET value = 0 WHERE variable = 'stop_count_1' LIMIT 1")
            con.commit()
            print "Roundcount to stop reached for machine 1!"

        #masin 2
        cur.execute("SELECT value FROM temp_config WHERE variable = 'stop_count_2'")
        result = cur.fetchone()
        res = int(result[0])
        if (res != 0) and (res <= roundcount_total_2) :
            GPIO.output(10, True)
            time.sleep(1.5)
            GPIO.output(10, False)
            cur.execute("UPDATE temp_config SET value = 0 WHERE variable = 'stop_count_2' LIMIT 1")
            con.commit()
            print "Roundcount to stop reached for machine 2!"
        
        #masin 3
        cur.execute("SELECT value FROM temp_config WHERE variable = 'stop_count_3'")
        result = cur.fetchone()
        res = int(result[0])
        if (res != 0) and (res <= roundcount_total_3) :
            GPIO.output(10, True)
            time.sleep(1.5)
            GPIO.output(10, False)
            cur.execute("UPDATE temp_config SET value = 0 WHERE variable = 'stop_count_3' LIMIT 1")
            con.commit()
            print "Roundcount to stop reached for machine 3!"
            
    except mysql.Error as err:
        print("ERROR: {}".format(err))
	
def check_for_remote_machine_stop(): #VAJA TEHA lisada kontroll kas kontrollitav muutuja on baasis olemas
    #kontrollime kas mysql baasis tabelis temp_config on muutuja remote_machine_stop vaartus suurem kui 0, kui on siis seiskame masina koheselt (lylitame relee sekundiks sisse).
    try:
        con = mysql.connect('localhost', local_server_mysql_user, local_server_mysql_password, local_server_database);	
        cur = con.cursor()
        
        #masin 1
        cur.execute("SELECT value FROM temp_config WHERE variable = 'remote_machine_stop_1'")
        result = cur.fetchone()
        res = int(result[0])
        if (res	== 1):
            GPIO.output(10, True)
            time.sleep(1.5)
            GPIO.output(10, False)
            cur.execute("UPDATE temp_config SET value = 0 WHERE variable = 'remote_machine_stop_1' LIMIT 1")
            con.commit()
            print "Machine number 1 remotely stopped!"
            
        #masin 2
        cur.execute("SELECT value FROM temp_config WHERE variable = 'remote_machine_stop_2'")
        result = cur.fetchone()
        res = int(result[0])
        if (res == 1):
            GPIO.output(10, True)
            time.sleep(1.5)
            GPIO.output(10, False)
            cur.execute("UPDATE temp_config SET value = 0 WHERE variable = 'remote_machine_stop_2' LIMIT 1")
            con.commit()
            print "Machine number 2 remotely stopped!"

        #masin 3
        cur.execute("SELECT value FROM temp_config WHERE variable = 'remote_machine_stop_3'")
        result = cur.fetchone()
        res = int(result[0])
        if (res == 1):
            GPIO.output(10, True)
            time.sleep(1.5)
            GPIO.output(10, False)
            cur.execute("UPDATE temp_config SET value = 0 WHERE variable = 'remote_machine_stop_3' LIMIT 1")
            con.commit()
            print "Machine number 3 remotely stopped!"

    except mysql.Error as err:
        print("ERROR: {}".format(err))

try:
    roundcount_total_value_read()    
    
    while True:
        try:
            #leds_1()
            #leds_2()
            #leds_3()
            
            #s.enter(1, 1, check_for_roundcount_total_reset, ())
            #s.enter(1, 1, check_for_remote_machine_stop, ())
            #s.enter(1, 1, check_for_stop_count, ())
            
            if (remote_server_is_reachable() == 1):
                s.enter(1, 1, remote_insert, ())
            else:
                s.enter(1, 1, local_insert, ())
            s.run()
            
            #print "----------------------------------------------------------------------- \n"
            
        except KeyboardInterrupt:
            print "\n Klaveri katkestus"
            GPIO.cleanup() # this ensures a clean exit 
            sys.exit(0)

except KeyboardInterrupt:
    print "\n Klaveri katkestus"
    GPIO.cleanup() # this ensures a clean exit 
    sys.exit(0)

#except:  
    # this catches ALL other exceptions including errors.  
    # You won't get any error messages for debugging  
    # so only use it once your code is working  
 #   print "Mingi muu jant!"   