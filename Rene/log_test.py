import os
import time

def log(message): #skripti tegevuse logimiseks
    aeg = (time.strftime("%H:%M:%S")) #kellaaeg
    kuupaev = (time.strftime("%d.%m.%Y")) #kuupaev
    os.chdir("/root/haine_log/") #liigume logide kausta
    f = open('log-m8.log','a+') #avame faili kirjutamiseks
    x = str(kuupaev + "," + aeg + "," + message + '\n') #loome stringi mille faili lisame
    f.write(x) #lisame loodud stringi faili
    f.close() #sulgeme faili
    
log("fakk off") #kutsume välja logimise funktsiooni mis kirjutab kaasa pandud info logifaili