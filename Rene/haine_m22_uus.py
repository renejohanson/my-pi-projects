import os
import sys
import RPi.GPIO as GPIO
import MySQLdb as mysql
import datetime, time, sched

GPIO.setmode(GPIO.BCM)
#seade 1
GPIO.setup(25, GPIO.IN) #ac1 in
GPIO.setup(24, GPIO.IN) #ac2 in
GPIO.setup(16, GPIO.IN) #rotation sensor in

#seade 2
GPIO.setup(23, GPIO.IN) #ac1 in
GPIO.setup(18, GPIO.IN) #ac2 in
GPIO.setup(20, GPIO.IN) #rotation sensor in

#seade 3
GPIO.setup(15, GPIO.IN) #ac1 in
GPIO.setup(14, GPIO.IN) #ac2 in
GPIO.setup(21, GPIO.IN) #rotation sensor in

#yldised muutujad
serial_number_1 = "21"
serial_number_2 = "22"
serial_number_3 = "23"
remote_server = '192.168.1.19'
remote_server_mysql_user = 'rene'
remote_server_mysql_password = 'Hullr007'
remote_server_database = 'haine'
local_server_mysql_user = 'root'
local_server_mysql_password = 'Hullr007'
local_server_database = 'haine'

s = sched.scheduler(time.time, time.sleep)

#seade 1
roundcount_1 = 0
roundcount_total_1 = 0
ac1_status_1 = not GPIO.input(25)
ac2_status_1 = not GPIO.input(24)

#seade 2
roundcount_2 = 0
roundcount_total_2 = 0
ac1_status_2 = not GPIO.input(23)
ac2_status_2 = not GPIO.input(18)

#seade 3
roundcount_3 = 0
roundcount_total_3 = 0
ac1_status_3 = not GPIO.input(15)
ac2_status_3 = not GPIO.input(14)

def gpio_22(channel):
    global roundcount_1
    global roundcount_total_1
    roundcount_1 += 1
    roundcount_total_1 += 1

def gpio_23(channel):
    global roundcount_2
    global roundcount_total_2
    roundcount_2 += 1
    roundcount_total_2 += 1

def gpio_13(channel):
    global roundcount_3
    global roundcount_total_3
    roundcount_3 += 1
    roundcount_total_3 += 1

GPIO.add_event_detect(16, GPIO.RISING, gpio_22)
GPIO.add_event_detect(20, GPIO.RISING, gpio_23)
GPIO.add_event_detect(21, GPIO.RISING, gpio_13) 

def remote_server_is_reachable():
    try:
        con = mysql.connect(remote_server, remote_server_mysql_user, remote_server_mysql_password, remote_server_database);    
        cur = con.cursor()
        cur.execute("SELECT VERSION()")
        result = cur.fetchone()
        if (result[0] != ""):
            return 1

    except mysql.Error as err:
        print("ERROR: {}".format(err))

def local_insert():
    try:
        global roundcount_1
        global roundcount_2
        global roundcount_3
        aeg = (time.strftime("%H:%M:%S"))
        kuupaev = (time.strftime("%d.%m.%Y"))
        con = mysql.connect('localhost', local_server_mysql_user, local_server_mysql_password, local_server_database);    
        cur = con.cursor()
        req1 = "INSERT INTO local_data (tulem, 220v, mootor, masina_number, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s)"
        cur.execute(req1, (roundcount_1, ac1_status_1, ac2_status_1, serial_number_1, aeg, kuupaev))
        req2 = "INSERT INTO local_data (tulem, 220v, mootor, masina_number, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s)"
        cur.execute(req2, (roundcount_2, ac1_status_2, ac2_status_2, serial_number_2, aeg, kuupaev))
        req3 = "INSERT INTO local_data (tulem, 220v, mootor, masina_number, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s)"
        cur.execute(req3, (roundcount_3, ac1_status_3, ac2_status_3, serial_number_3, aeg, kuupaev))
        con.commit()
        roundcount_1 = 0
        roundcount_2 = 0
        roundcount_3 = 0
        
    except mysql.Error as err:
        print("ERROR: {}".format(err))
        
def remote_insert():
    try:
        global roundcount_1
        global roundcount_2
        global roundcount_3 
        aeg = (time.strftime("%H:%M:%S"))
        kuupaev = (time.strftime("%d.%m.%Y"))
        con = mysql.connect(remote_server, remote_server_mysql_user, remote_server_mysql_password, remote_server_database);    
        cur = con.cursor()
        req = "INSERT INTO local_data (tulem, 220v, mootor, masina_number, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s)"
        cur.execute(req, (roundcount_1, ac1_status_1, ac2_status_1, serial_number_1, aeg, kuupaev))
        req = "INSERT INTO local_data (tulem, 220v, mootor, masina_number, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s)"
        cur.execute(req, (roundcount_2, ac1_status_2, ac2_status_2, serial_number_2, aeg, kuupaev))
        req = "INSERT INTO local_data (tulem, 220v, mootor, masina_number, aeg, kuupaev) VALUES (%s, %s, %s, %s, %s, %s)"
        cur.execute(req, (roundcount_3, ac1_status_3, ac2_status_3, serial_number_3, aeg, kuupaev))
        con.commit()
        roundcount_1 = 0
        roundcount_2 = 0
        roundcount_3 = 0
        
    except mysql.Error as err:
        print("ERROR: {}".format(err))
        

while True:
    try:
        if (remote_server_is_reachable() == 1):
            s.enter(1, 1, remote_insert, ())
        else:
            s.enter(1, 1, local_insert, ())
        s.run()
            
    except KeyboardInterrupt:
        print "\n Klaveri katkestus"
        GPIO.cleanup()
        sys.exit(0)

#except:  
    # this catches ALL other exceptions including errors.  
    # You won't get any error messages for debugging  
    # so only use it once your code is working  
 #   print "Mingi muu jant!"   